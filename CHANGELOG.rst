Changelog
=========

0.3.0 - 2020-09-21
------------------
Added
~~~~~
- Support for binning 1D axis
- Better support for Raman retrievals
- Simple moleculare calculation for atmospheric properties in the troposphere.

Fixed
~~~~~
- Klett retrieval reference value choice

0.2.2 - 2019-07-16
------------------
Added
~~~~~
- Support for both 1D and 2D arrays
- Many undocumented changes

0.1.0 - 2015-01-01
------------------
Added
~~~~~
- Initial release
